async function currencyChange() {
    let dropdown = document.getElementById("currency");
    let currentCurrency = dropdown.options[dropdown.selectedIndex].value;
    let pricesWithCurrency = document.getElementsByClassName("price");
    let previousCurrency = pricesWithCurrency[0].innerHTML.split(" ")[1];

    let requestOptions = {
        method: 'GET', redirect: 'follow'
    };
    const response = await fetch("https://api.currencyapi.com/v3/latest?apikey=r455t6ISHM3dEqprlKoha2Ii1vM8tOGvJYfv8Rb5&base_currency=" + previousCurrency, requestOptions)
        .catch(error => console.log('error', error));
    const json = await response.json();

    let responseCurrency = json.data[currentCurrency];

    for (let i = 0; i < pricesWithCurrency.length; i++) {
        let price = parseInt(pricesWithCurrency[i].innerHTML.split(" ")[0]);

        price *= responseCurrency.value;
        price = price.toFixed(2);

        pricesWithCurrency[i].innerHTML = price + " " + responseCurrency.code;
    }

}